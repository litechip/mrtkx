var proxies = 
[
    'http://104.46.52.26:58099',
];


function randomProxy() {
	return proxies[Math.floor(Math.random()*proxies.length)];
}

function allProxies() {
	return proxies;
}

function getAuthProxyByIpAndPort(ipPort) {
	
    for (var i = proxies.length - 1; i >= 0; i--) {
        if (proxies[i].indexOf(ipPort) > -1) {
            return proxies[i];
        }
    }
}

function isProxyInRange(proxy, startIdx, endIdx) {

    var position = proxies.indexOf(proxy);
    var result = startIdx <= position && position < endIdx; 
        
    // if (result) {
    //     console.log(proxy, startIdx, endIdx, position, result);
    // }

    return result;
}

module.exports = {
	randomProxy: randomProxy,
	getAuthProxyByIpAndPort: getAuthProxyByIpAndPort,
    allProxies: allProxies,
    isProxyInRange: isProxyInRange,
	allProxies: allProxies,
}