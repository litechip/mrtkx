var randomUseragent = require('random-useragent');
const puppeteer = require('puppeteer');
const proxyChain = require('proxy-chain');

// https://intoli.com/blog/not-possible-to-block-chrome-headless/
// https://github.com/GoogleChrome/puppeteer/issues/1641
// This is where we'll put the code to get around the tests.
const preparePageForTests = async (page) => {
  
  // anti-spy measures
  // Pass the User-Agent Test.
  const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
     'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
  
  // const userAgent = randomUseragent.getRandom();
  await page.setUserAgent(userAgent);

  // Pass the Webdriver Test.
  await page.evaluateOnNewDocument(() => {
    Object.defineProperty(navigator, 'webdriver', {
      get: () => false,
    });
  });

  // Pass the Chrome Test.
  await page.evaluateOnNewDocument(() => {
    // We can mock this in as much depth as we need for the test.
    window.navigator.chrome = {
      runtime: {},
      // etc.
    };
  });

  // Pass the Permissions Test.
  await page.evaluateOnNewDocument(() => {
    const originalQuery = window.navigator.permissions.query;
    return window.navigator.permissions.query = (parameters) => (
      parameters.name === 'notifications' ?
        Promise.resolve({ state: Notification.permission }) :
        originalQuery(parameters)
    );
  });

  // Pass the Plugins Length Test.
  await page.evaluateOnNewDocument(() => {
    // Overwrite the `plugins` property to use a custom getter.
    Object.defineProperty(navigator, 'plugins', {
      // This just needs to have `length > 0` for the current test,
      // but we could mock the plugins too if necessary.
      get: () => [1, 2, 3, 4, 5],
    });
  });

  // Pass the Languages Test.
  await page.evaluateOnNewDocument(() => {
    // Overwrite the `plugins` property to use a custom getter.
    Object.defineProperty(navigator, 'languages', {
      get: () => ['en-US', 'en'],
    });
  });
}

const getBrowserArguments = async (options) => {
  // options = options || {};
  // const oldProxy = options.proxy;
  // console.log(oldProxy);
  // const newProxyUrl = await proxyChain.anonymizeProxy(oldProxy);
  // console.log(oldProxy);
  // console.log(newProxyUrl);

  return {
    headless: true,
    args: [
      //`--proxy-server=${newProxyUrl}`, 
      `--window-size=1920,1080`,
      `--no-sandbox`,
    ],
  }
}


const getPageLoadParams = () => {
  return {
    waitUntil: 'networkidle2',
    timeout: 0
  };
}

const getBrowser = async (options) => {
  
  options = options || {};

  const instanceArgs = await getBrowserArguments(options);
  const browser = await puppeteer.launch(instanceArgs);

  return browser;
}

const getSpyPage = async (browser, options) => {
  options = options || {};

  const page = await browser.newPage();
  page.setViewport({width: 1920, height: 1080});

  await preparePageForTests(page);

  if (options.proxy) {
    await setProxyForPage(page, options.proxy);
  }

  return page;
}


const getBrowserPage = async (options) => {
  
  const browser = await getBrowser(options);
  const page = await getSpyPage(browser);

  return page;
}

////////////// IMAGE DOWNLOAD
const getDataUrlThroughFetch = async (selector, options = {}) => {
  const image = document.querySelector(selector);
  const url = image.src;

  const response = await fetch(url, options);
  if (!response.ok) {
    throw new Error(`Could not fetch image, (status ${response.status}`);
  }
  const data = await response.blob();
  const reader = new FileReader();
  return new Promise((resolve) => {
    reader.addEventListener('loadend', () => resolve(reader.result));
    reader.readAsDataURL(data);
  });
};

////////////////////// UTILS

const getRandomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const waitRandomOnPage = async (page, min, max) => {
    await page.waitFor(getRandomInt(min, max));
    
}    

module.exports = {
  getPageLoadParams: getPageLoadParams,
  getBrowserArguments: getBrowserArguments,
  getBrowserPage: getBrowserPage,
  getBrowser: getBrowser,
  getSpyPage: getSpyPage,
  getRandomInt: getRandomInt,
  waitRandomOnPage: waitRandomOnPage,
  getDataUrlThroughFetch: getDataUrlThroughFetch,
  preparePageForTests: preparePageForTests,
}