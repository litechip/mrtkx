const headless = require('./headless');
var sleep = require('sleep');

const commandLineArgs = require('command-line-args')

const optionDefinitions = [
  { name: 'address', alias: 'a', type: String},
  { name: 'cpus', alias: 'c', type: String},
]

const options = commandLineArgs(optionDefinitions);
console.log(options);

async function runBrowserSession(options) {

    try {
        var url = 'https://webdollar.io';
        console.log(url);
        const browser = await headless.getBrowser();
        const page = await headless.getSpyPage(browser);
        await page.goto(url, headless.getPageLoadParams());


        // listen on our side for changed values
        page.on('console', msg => {
          var text = msg.text();
          console.log(text);
        });

        await page.evaluate( (options) => {
            var WebDollar = window.WebDollar || {};
            window.minerAddress = '';

            function sleep(ms) {
              return new Promise(resolve => setTimeout(resolve, ms));
            }


            var downloadAdress = function() {
                // var answer = await WebDollar.Blockchain.Wallet.exportAddressToJSON(window.minerAddress);

                // if (answer.result){

                //     let addressFile = new Blob([JSON.stringify(answer.data)], {type: "application/json;charset=utf-8"});
                //     let fileName = "WEBD$" + WebDollar.Blockchain.Wallet.getUnencodedAddress(this.address).toString("hex") + ".webd";
                //     FileSaver.saveAs(addressFile, fileName);

                // } else {
                //     alert(answer.message)
                // }  
            }

            var watchAdresses =  function() {
                window.minerAddress = WebDollar.Blockchain.Mining.minerAddressBase;
                
                WebDollar.StatusEvents.on("mining/miner-address-changed", (minerAddress)=>{
                    window.minerAddress = minerAddress;
                });
                
            }
            
            var startMining = function() {
                console.error('================START MINING=====================');
                var workers = parseInt(options.cpus);
                console.error('================SETTING WORKERS=====================' + workers);
                WebDollar.Blockchain.Mining.setWorkers(workers);
                
                console.error('================SETTING ADDRESS=====================' + options.address);
                WebDollar.Blockchain.Mining.minerAddress = options.address;
            }

            var currentMiningStats = function() {
                console.error('================AFTER SETTING ADDRESS=====================' + WebDollar.Blockchain.Mining.minerAddress);
            }

            var isMiningCond1 = WebDollar.Blockchain.Mining.workers.workers;
            var isMiningCond2 = WebDollar.Blockchain.loaded;
            
            // while(WebDollar.Blockchain.loaded !== true) {
            //     console.log('===================BLOCKCHAIN NOT LOADED SLEEPING ===========');
            //     sleep(10000);
            // }

            startMining();
            currentMiningStats();

            setInterval(startMining, 10000);
            setInterval(currentMiningStats, 10000);

        }, options);

    } catch(error) {
      console.log(error, error.stack);
      if (page) {
        await page.close();
      }
    }
}


(async function(){

  await runBrowserSession(options);

})();